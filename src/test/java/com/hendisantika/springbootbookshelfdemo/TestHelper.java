package com.hendisantika.springbootbookshelfdemo;

import com.hendisantika.springbootbookshelfdemo.domain.AuthorEntity;
import com.hendisantika.springbootbookshelfdemo.domain.BookEntity;
import com.hendisantika.springbootbookshelfdemo.domain.BookReviewEntity;
import com.hendisantika.springbootbookshelfdemo.internal.QueryStatementHolder;
import net.ttddyy.dsproxy.QueryCountHolder;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:55
 * To change this template use File | Settings | File Templates.
 */
@Transactional
public class TestHelper {
    @PersistenceContext
    private EntityManager entityManager;

    public void doInTransaction(final Consumer<EntityManager> consumer) {
        consumer.accept(entityManager);
    }

    public <T> T doInTransaction(final Function<EntityManager, T> function) {
        return function.apply(entityManager);
    }

    public void resetDB() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaDelete<AuthorEntity> authorDelete = cb.createCriteriaDelete(AuthorEntity.class);
        authorDelete.from(AuthorEntity.class);
        CriteriaDelete<BookEntity> bookDelete = cb.createCriteriaDelete(BookEntity.class);
        bookDelete.from(BookEntity.class);
        CriteriaDelete<BookReviewEntity> reviewDelete = cb.createCriteriaDelete(BookReviewEntity.class);
        reviewDelete.from(BookReviewEntity.class);
        entityManager.createQuery(reviewDelete).executeUpdate();
        entityManager.createQuery(bookDelete).executeUpdate();
        entityManager.createQuery(authorDelete).executeUpdate();
        QueryCountHolder.clear();
        QueryStatementHolder.clear();
    }

}