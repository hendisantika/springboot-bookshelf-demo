package com.hendisantika.springbootbookshelfdemo;

import com.hendisantika.springbootbookshelfdemo.internal.DataSourceBeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:59
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class TestConfiguration {
    @Bean
    public DataSourceBeanPostProcessor dataSourceBeanPostProcessor() {
        return new DataSourceBeanPostProcessor();
    }

    @Bean
    public TestHelper testHelper() {
        return new TestHelper();
    }
}