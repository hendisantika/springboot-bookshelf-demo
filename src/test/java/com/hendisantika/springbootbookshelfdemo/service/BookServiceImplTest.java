package com.hendisantika.springbootbookshelfdemo.service;

import com.hendisantika.springbootbookshelfdemo.TestConfiguration;
import com.hendisantika.springbootbookshelfdemo.TestHelper;
import com.hendisantika.springbootbookshelfdemo.domain.*;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.UUID;

import static com.hendisantika.springbootbookshelfdemo.QueryAssertions.*;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 15:00
 * To change this template use File | Settings | File Templates.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@Import({TestConfiguration.class})
public class BookServiceImplTest {
    @Autowired
    private TestHelper testHelper;

    @Autowired
    private BookService underTest;

    @After
    public void tearDown() {
        testHelper.resetDB();
    }

    @Test
    public void testGetBooksShouldTriggerOneSelectOnly() {
        // given
        UUID effectiveJavaId = testHelper.doInTransaction(entityManager -> {
            AuthorEntity oneAuthor = new AuthorEntity("Someone..", "There was an author..");
            BookEntity effectiveJava = new BookEntity("Effective Java", 10, "12131231312", LanguageType.ENGLISH, oneAuthor);
            BookReviewEntity review1 = new BookReviewEntity();
            BookReviewEntity review2 = new BookReviewEntity();
            effectiveJava.addReview(review1);
            effectiveJava.addReview(review2);
            BookEntity effectiveJava2 = new BookEntity("Effective Java 2nd edition", 11, "12131231313", LanguageType.ENGLISH, oneAuthor);
            BookEntity cleanCode = new BookEntity("Clean code", 200, "568751213", LanguageType.ENGLISH, oneAuthor);
            entityManager.persist(oneAuthor);
            return effectiveJava.getId();
        });
        // when
        Collection<BookView> result = underTest.getBooks();
        // then
        assertThat(result).hasSize(3);
        assertThat(result.stream().filter(view -> effectiveJavaId.equals(view.getBookId())).findFirst().get().getReviewCount()).isEqualTo(2);
        assertSelectCount(1);
    }

    @Test
    public void testGetBookShouldTriggerOneSelectOnly() {
        // given
        UUID effectiveJavaId = testHelper.doInTransaction(entityManager -> {
            AuthorEntity oneAuthor = new AuthorEntity("Someone..", "There was an author..");
            BookEntity effectiveJava = new BookEntity("Effective Java", 10, "12131231312", LanguageType.ENGLISH, oneAuthor);
            BookReviewEntity review1 = new BookReviewEntity();
            effectiveJava.addReview(review1);
            entityManager.persist(oneAuthor);
            return effectiveJava.getId();
        });
        // when
        BookView result = underTest.getBook(effectiveJavaId);
        // then
        assertThat(result).isNotNull();
        assertThat(result.getReviewCount()).isEqualTo(1);
        assertSelectCount(2);
    }

    @Test
    public void testUpdateNameShouldTriggerOneSelectAndOneUpdateOnly() {
        // given
        UUID effectiveJavaId = testHelper.doInTransaction(entityManager -> {
            AuthorEntity oneAuthor = new AuthorEntity("Someone..", "There was an author..");
            BookEntity effectiveJava = new BookEntity("Effective Java", 10, "12131231312", LanguageType.ENGLISH, oneAuthor);
            entityManager.persist(oneAuthor);
            return effectiveJava.getId();
        });
        // when
        underTest.changeName(effectiveJavaId, "Effective Java 2nd Edition");
        // then
        assertSelectCount(1);
        assertUpdateCount(1);
    }

    @Test
    public void testGetSimpleBookShouldTriggerOneSelectOnly() {
        // given
        UUID effectiveJavaId = testHelper.doInTransaction(entityManager -> {
            AuthorEntity oneAuthor = new AuthorEntity("Someone..", "There was an author..");
            BookEntity effectiveJava = new BookEntity("Effective Java", 10, "12131231312", LanguageType.ENGLISH, oneAuthor);
            entityManager.persist(oneAuthor);
            return effectiveJava.getId();
        });
        // when
        SimpleBookView result = underTest.getSimpleBook(effectiveJavaId);
        // then
        assertThat(result).isNotNull();
        assertSelectCount(1);
        assertSelectionFields("id", "name");
    }
}