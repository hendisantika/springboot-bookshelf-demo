package com.hendisantika.springbootbookshelfdemo.service;

import com.hendisantika.springbootbookshelfdemo.TestHelper;
import com.hendisantika.springbootbookshelfdemo.domain.AuthorEntity;
import com.hendisantika.springbootbookshelfdemo.domain.AuthorView;
import org.assertj.core.api.Assertions;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

import static com.hendisantika.springbootbookshelfdemo.QueryAssertions.assertSelectCount;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:55
 * To change this template use File | Settings | File Templates.
 */
@SpringBootTest
@RunWith(SpringRunner.class)
@Import({TestConfiguration.class})
public class AuthorServiceImplTest {
    @Autowired
    private TestHelper testHelper;

    @Autowired
    private AuthorService underTest;

    @After
    public void tearDown() {
        testHelper.resetDB();
    }

    @Test
    public void testGetAuthorsShouldOnlyTriggerOneSelect() {
        // given
        testHelper.doInTransaction(entityManager -> {
            AuthorEntity joshua = new AuthorEntity("Joshua Bloch", "I'm the god of Java :-)");
            entityManager.persist(joshua);
        });
        // when
        Collection<AuthorView> result = underTest.getAuthors();
        // then
        Assertions.assertThat(result).hasSize(1);
        assertSelectCount(2);
    }
}