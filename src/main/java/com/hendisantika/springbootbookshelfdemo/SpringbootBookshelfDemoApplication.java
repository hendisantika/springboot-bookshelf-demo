package com.hendisantika.springbootbookshelfdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootBookshelfDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootBookshelfDemoApplication.class, args);
    }

}

