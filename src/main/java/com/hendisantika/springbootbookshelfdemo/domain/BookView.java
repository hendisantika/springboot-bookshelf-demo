package com.hendisantika.springbootbookshelfdemo.domain;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
public class BookView {
    private UUID bookId;
    private int pageCount;
    private String isbn;
    private LanguageType language;
    private int reviewCount;

    public BookView(final BookEntity entity) {
        this.bookId = entity.getId();
        this.pageCount = entity.getPageCount();
        this.isbn = entity.getIsbn();
        this.language = entity.getLanguage();
        this.reviewCount = entity.getReviewCount();
    }

    public UUID getBookId() {
        return bookId;
    }

    public int getPageCount() {
        return pageCount;
    }

    public String getIsbn() {
        return isbn;
    }

    public LanguageType getLanguage() {
        return language;
    }

    public int getReviewCount() {
        return reviewCount;
    }

}
