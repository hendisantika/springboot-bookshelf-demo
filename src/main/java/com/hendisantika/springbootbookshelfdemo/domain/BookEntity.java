package com.hendisantika.springbootbookshelfdemo.domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:33
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "books")
public class BookEntity {
    @Id
    private UUID id;

    @Column(name = "name")
    private String name;

    @Column(name = "page_count")
    private Integer pageCount;

    @Column(name = "isbn")
    private String isbn;

    @Column(name = "language")
    @Enumerated(EnumType.STRING)
    private LanguageType language;

    @ManyToOne(fetch = FetchType.LAZY)
    private AuthorEntity author;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY, mappedBy = "book")
    private Set<BookReviewEntity> reviews = new HashSet<>();

    BookEntity() {
    }

    public BookEntity(final String name, final int pageCount, final String isbn, final LanguageType language, final AuthorEntity author) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.pageCount = pageCount;
        this.isbn = isbn;
        this.language = language;
        this.author = author;
        this.author.addBook(this);
    }

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Integer getPageCount() {
        return pageCount;
    }

    public void setPageCount(final Integer pageCount) {
        this.pageCount = pageCount;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(final String isbn) {
        this.isbn = isbn;
    }

    public LanguageType getLanguage() {
        return language;
    }

    public void setLanguage(final LanguageType language) {
        this.language = language;
    }

    public AuthorEntity getAuthor() {
        return author;
    }

    void setAuthor(final AuthorEntity author) {
        this.author = author;
    }

    public void addReview(final BookReviewEntity review) {
        reviews.add(review);
        review.setBook(this);
    }

    public void removeReview(final BookReviewEntity review) {
        reviews.remove(review);
    }

    public Set<BookReviewEntity> getReviews() {
        return Collections.unmodifiableSet(reviews);
    }

    public int getReviewCount() {
        return reviews.size();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(id).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BookEntity other = (BookEntity) obj;
        return new EqualsBuilder().append(id, other.id).isEquals();
    }

}