package com.hendisantika.springbootbookshelfdemo.domain;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:32
 * To change this template use File | Settings | File Templates.
 */
public class AuthorView {
    private UUID authorId;
    private String name;
    private String introduction;

    public AuthorView(final AuthorEntity entity) {
        this.authorId = entity.getId();
        this.name = entity.getName();
        this.introduction = entity.getIntroduction();
    }

    public UUID getAuthorId() {
        return authorId;
    }

    public String getName() {
        return name;
    }

    public String getIntroduction() {
        return introduction;
    }
}
