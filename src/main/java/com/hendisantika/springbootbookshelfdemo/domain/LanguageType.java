package com.hendisantika.springbootbookshelfdemo.domain;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:36
 * To change this template use File | Settings | File Templates.
 */
public enum LanguageType {
    ENGLISH, BAHASA, ARABIC
}
