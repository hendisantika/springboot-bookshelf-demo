package com.hendisantika.springbootbookshelfdemo.domain;

import java.util.UUID;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:41
 * To change this template use File | Settings | File Templates.
 */
public class SimpleBookView {
    private UUID bookId;
    private String name;

    public SimpleBookView(BookEntity entity) {
        this.bookId = entity.getId();
        this.name = entity.getName();
    }
}
