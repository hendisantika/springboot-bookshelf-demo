package com.hendisantika.springbootbookshelfdemo.internal;

import net.ttddyy.dsproxy.QueryInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:49
 * To change this template use File | Settings | File Templates.
 */
public abstract class QueryStatementHolder {
    private static ThreadLocal<List<QueryInfo>> queryInfos = ThreadLocal.withInitial(() -> new ArrayList<>());

    public static void add(List<QueryInfo> infos) {
        queryInfos.get().addAll(infos);
    }

    public static List<QueryInfo> get() {
        return queryInfos.get();
    }

    public static void clear() {
        queryInfos.get().clear();
    }
}