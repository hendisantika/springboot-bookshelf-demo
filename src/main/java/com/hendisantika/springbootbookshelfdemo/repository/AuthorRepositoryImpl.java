package com.hendisantika.springbootbookshelfdemo.repository;

import com.hendisantika.springbootbookshelfdemo.domain.AuthorEntity;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:43
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class AuthorRepositoryImpl implements AuthorRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Collection<AuthorEntity> findAll() {
        return entityManager.createQuery("FROM AuthorEntity", AuthorEntity.class).getResultList();
    }
}
