package com.hendisantika.springbootbookshelfdemo.repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:42
 * To change this template use File | Settings | File Templates.
 */

import com.hendisantika.springbootbookshelfdemo.domain.AuthorEntity;

import java.util.Collection;

/**
 * Repository to query Authors
 */

public interface AuthorRepository {
    /**
     * Returns all the AuthorEntities
     *
     * @return all the AuthorEntities
     */
    Collection<AuthorEntity> findAll();
}
