package com.hendisantika.springbootbookshelfdemo.repository;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:43
 * To change this template use File | Settings | File Templates.
 */

import com.hendisantika.springbootbookshelfdemo.domain.BookEntity;

import java.util.Collection;
import java.util.UUID;

/**
 * Repository to query Books
 */
public interface BookRepository {
    /**
     * Returns all the BookEntities
     *
     * @return all the BookEntities
     */
    Collection<BookEntity> findAll();

    /**
     * Returns the BookEntity with the corresponding id
     *
     * @param id the id of the searched BookEntity. Should be not null.
     * @return the BookEntity with the corresponding id
     */
    BookEntity findById(UUID id);
}
