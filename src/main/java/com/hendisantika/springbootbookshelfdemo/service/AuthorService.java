package com.hendisantika.springbootbookshelfdemo.service;

import com.hendisantika.springbootbookshelfdemo.domain.AuthorView;

import java.util.Collection;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:45
 * To change this template use File | Settings | File Templates.
 * <p>
 * <p>
 * Service for Authors
 */
public interface AuthorService {

    /**
     * Returns all the Authors
     *
     * @return all the Authors
     */
    Collection<AuthorView> getAuthors();
}
