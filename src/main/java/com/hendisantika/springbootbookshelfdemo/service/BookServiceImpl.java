package com.hendisantika.springbootbookshelfdemo.service;

import com.hendisantika.springbootbookshelfdemo.domain.BookEntity;
import com.hendisantika.springbootbookshelfdemo.domain.BookView;
import com.hendisantika.springbootbookshelfdemo.domain.SimpleBookView;
import com.hendisantika.springbootbookshelfdemo.repository.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.UUID;

import static java.util.stream.Collectors.toList;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:47
 * To change this template use File | Settings | File Templates.
 */
@Service
public class BookServiceImpl implements BookService {
    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private BookServiceHelper helper;

    @Override
    @Transactional
    public Collection<BookView> getBooks() {
        return bookRepository.findAll().stream().map(BookView::new).collect(toList());
    }

    @Override
    @Transactional
    public BookView getBook(final UUID id) {
        return new BookView(bookRepository.findById(id));
    }

    @Override
    @Transactional
    public SimpleBookView getSimpleBook(UUID id) {
        return new SimpleBookView(bookRepository.findById(id));
    }

    @Override
    public void changeName(final UUID id, final String name) {
        BookEntity book = bookRepository.findById(id);
        helper.updateName(book, name);
    }

}