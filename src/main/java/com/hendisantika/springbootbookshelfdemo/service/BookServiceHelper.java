package com.hendisantika.springbootbookshelfdemo.service;

import com.hendisantika.springbootbookshelfdemo.domain.BookEntity;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:48
 * To change this template use File | Settings | File Templates.
 */
@Component
@Transactional
public class BookServiceHelper {
    @PersistenceContext
    private EntityManager entityManager;

    public BookEntity updateName(final BookEntity book, final String newName) {
        book.setName(newName);
        return entityManager.merge(book);
    }
}