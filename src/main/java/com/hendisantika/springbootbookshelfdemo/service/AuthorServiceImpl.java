package com.hendisantika.springbootbookshelfdemo.service;

import com.hendisantika.springbootbookshelfdemo.domain.AuthorView;
import com.hendisantika.springbootbookshelfdemo.repository.AuthorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

import static java.util.stream.Collectors.toList;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-bookshelf-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2019-02-02
 * Time: 14:46
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {
    @Autowired
    private AuthorRepository authorRepository;

    @Override
    public Collection<AuthorView> getAuthors() {
        return authorRepository.findAll().stream().map(AuthorView::new).collect(toList());
    }
}